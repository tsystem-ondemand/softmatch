module.exports = (function() {

var objectFlip = function(object) {
    var newObj = {};
    Object.keys(object).forEach( function(elem) {
        newObj[object[elem]] = elem;
    });
    return newObj;
};

var arrayUniqueSort = function (array) {
    var sorted = array.sort();

    return sorted.filter( function(value, i) {
        return (i === 0) || (value !== sorted[i-1]);
    });
};

var keymaps = [
    //русские буквы на клавиатуре в английском регистре ввода
    {
        '~':'е', '`':'е', 'ё':'е',
        q:'й', w:'ц', e:'у', r:'к', t:'е', y:'н', u:'г', i:'ш', o:'щ', p:'з', '{':'х', '[':'х', '}':'ъ', ']':'ъ',
        a:'ф', s:'ы', d:'в', f:'а', g:'п', h:'р', j:'о', k:'л', l:'д', ':':'ж', ';':'ж', '"':'э', "'":'э',
        z:'я', x:'ч', c:'с', v:'м', b:'и', n:'т', m:'ь', '<':'б', ',':'б', '>':'ю', '.':'ю'
    }
];

//добавляем обратное преобразование
keymaps = [].concat(keymaps, keymaps.map(objectFlip));

var makeSoft = function(string) {
    var str = string.trim().toLowerCase();
    var strlen = str.length;

    var results = keymaps.map(function (keymap) {
        var result = '';
        for (var i = 0; i < strlen; ++i) {
            var ch = str.charAt(i);
            if (ch) {
                result += (keymap[ch] ? keymap[ch] : ch);
            }
        }
        return result;
    });

    //нормализация
    results = results.join('|').split('|');
    results = results.filter( function(value) {
        return value != null && value != '';
    });
    results = arrayUniqueSort(results);

    return results.join('|');
};

//ищем подстроку запроса в произвольном месте
var match = function(softQuery, softIndex) {
    return softQuery.split('|').some( function(q) {
        return softIndex.indexOf(q) !== -1;
    });
};

//ищем подстроку запрос только с начала строк
var matchStrict = function(softQuery, softIndex) {
    return softQuery.split('|').some( function(q) {
        return ('|' + softIndex).indexOf('|' + q) !== -1;
    });
};

return {
    makeSoft: makeSoft,
    match: match,
    matchStrict: matchStrict
};

})();
